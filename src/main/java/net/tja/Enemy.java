package net.tja;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import net.tja.map.Map.Vertex;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Enemy extends Entity {
    @Setter
    private int maxHp = 100;
    private int hp;
    private final int vertexNumber;
    private final boolean flying;
    @Setter
    private int actualPosOnPath;
    @Setter
    private List<Vertex> mobPath;

    public Enemy(String name, int vertexNumber, boolean flying) {
        this(name, 100, vertexNumber, flying);
    }

    public Enemy(String name, int hp, int vertexNumber, boolean flying) {
        this(name, hp, vertexNumber, flying, null);
    }

    public Enemy(String name, int hp, int vertexNumber, boolean flying, List<Vertex> mobPath) {
        super(name);
        this.hp = hp;
        this.vertexNumber = vertexNumber;
        this.flying = flying;
        this.mobPath = mobPath;
    }

    public void mobMoved() {
        actualPosOnPath++;
    }

    public void setHp(int hp) {
        if (hp < 0) {
            this.hp = 0;
        } else {
            this.hp = hp;
        }
    }
}