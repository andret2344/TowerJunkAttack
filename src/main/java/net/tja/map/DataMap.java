/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tja.map;

import lombok.Getter;
import net.tja.Location;

public class DataMap {

    private final int[][] map;
    @Getter
    private final int sizeX;
    @Getter
    private final int sizeY;

    DataMap(int[][] map) {
        this.map = map;
        sizeX = map.length;
        sizeY = map[0].length;
    }

    public int getMapSize() {
        return Math.min(sizeX, sizeY);
    }

    public synchronized void setCell(DataMapCell cell) {
        map[cell.getLocation().getX()][cell.getLocation().getY()] = cell.getData();
    }

    public synchronized void setCellData(Location loc, int data) {
        map[loc.getX()][loc.getY()] = data;
    }

    public synchronized void setCellData(int x, int y, int data) {
        map[x][y] = data;
    }

    public synchronized DataMapCell getCell(Location loc) {
        DataMapCell cell = new DataMapCell(loc);
        cell.setData(-1);
        if (loc.getX() >= 0 && loc.getX() < map.length
                && loc.getY() >= 0 && loc.getY() < map[0].length) {
            cell.setData(map[loc.getX()][loc.getY()]);
        }
        return cell;
    }

    public synchronized DataMapCell getCell(int x, int y) {
        DataMapCell cell = new DataMapCell(x, y);
        cell.setData(-1);
        if (x >= 0 && x < map.length && y >= 0 && y < map[0].length) {
            cell.setData(map[x][y]);
        }
        return cell;
    }

    public synchronized int getCellData(Location loc) {
        return map[loc.getX()][loc.getY()];
    }

    public synchronized int getCellData(int x, int y) {
        return map[x][y];
    }
}
