package net.tja.map;

import lombok.Value;
import lombok.experimental.NonFinal;
import net.tja.*;
import net.tja.panel.PanelManager;
import net.tja.projectile.Projectile;
import net.tja.tower.Tower;
import net.tja.tower.TowerType;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicInteger;

public class Map extends JPanel {

    private int blockSize;
    private String name;
    private final Timer mobUpdateTimer = new Timer();
    private final Timer waveTimer = new Timer();
    private final Timer towerUpdateTimer = new Timer();
    private final Timer procjectileTimer = new Timer();
    private boolean isVaveCompleted = true;
    private int waveNum;
    private AtomicInteger numOfThreadRuning;
    private Location startPoint, endPoint;

    private final int waveStartAfter = 10000;
    private final int waveAfter = 3000;
    private final int updateMobEvery = 100;
    private final int updateTowerEvery = 100;
    private final int updateProjectileEvery = 50;
    private List<Enemy> enemyList = Collections.synchronizedList(new LinkedList<>());
    private final List<Enemy> nextWave = new LinkedList<>();
    private List<Tower> towerList = Collections.synchronizedList(new LinkedList<>());
    private List<Tower> towerAddList = Collections.synchronizedList(new LinkedList<>());
    private List<Projectile> projectileList = Collections.synchronizedList(new LinkedList<>());
    private List<Projectile> projectileListAdd = Collections.synchronizedList(new LinkedList<>());
    private final List<TowerType> availableTowers = new ArrayList<>();
    private final Game game;
    private EntityMap entMap;
    private DamageMap dmgMap;
    private DataMap dataMap;

    @Value
    public class Vertex {
        public int x, y;
        @NonFinal
        public int costOfGettingHere = Integer.MAX_VALUE;
        public int costOfEnteringThisOne;

        public Vertex(int x, int y, int costOfEnteringThisOne) {
            this.x = x;
            this.y = y;
            this.costOfEnteringThisOne = costOfEnteringThisOne;
        }
    }

    public Map(Game game, String name, double size, int[][] map) {
        setSize(TowerJunkAttack.getSize(), TowerJunkAttack.getSize());
        this.game = game;
        this.name = name;
        dataMap = new DataMap(map);
        blockSize = (int) (size / dataMap.getMapSize());
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == 3) {
                    endPoint = new Location(i, j);
                } else if (map[i][j] == 2) {
                    startPoint = new Location(i, j);
                }
            }
        }
        entMap = new EntityMap(map.length, map[0].length);
        dmgMap = new DamageMap(map.length, map[0].length, dataMap);
        availableTowers.add(TowerType.BASE);
        availableTowers.add(TowerType.QUICK);
        availableTowers.add(TowerType.STRONG);
        availableTowers.add(TowerType.WIDE);

        numOfThreadRuning = new AtomicInteger(0);

        // TODO: Change how to wave start - depend actual wave on next one
        mobUpdateTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Lets find mobs
                if (isVaveCompleted || game.isPaused()) {
                    return;
                }
                numOfThreadRuning.addAndGet(1);
                List<Enemy> enemiesListTmp = new LinkedList<>(enemyList);
                if (nextWave.size() > 0) {
                    Enemy en = nextWave.get(0);
                    LinkedList<Vertex> path = generatePaths();
                    Location eneStart = new Location(path.get(0).x, path.get(0).y);
                    en.setLocation(eneStart);
                    en.setMobPath(path);
                    enemiesListTmp.add(en);
                    entMap.setCell(en);
                    nextWave.remove(0);
                }

                List<Enemy> enemiesToMove = new LinkedList<>(enemiesListTmp);

                // now chceck if they can move
                // if they can move then and remove from list
                // if they cant just skip
                // do while list betwwen iterations have the same length
                int prevSize = enemiesToMove.size();
                if (prevSize == 0) {
                    isVaveCompleted = true;
                    Log.info("Ended wave: " + waveNum);
                }
                if (prevSize > 0) {
                    do {
                        prevSize = enemiesToMove.size();
                        for (int i = 0; i < enemiesToMove.size(); ++i) {
                            Enemy enemy = enemiesToMove.get(i);
                            if (enemy.getHp() <= 0) {
                                if (dataMap.getCellData(enemy.getLocation()) == 11) {
                                    dataMap.setCellData(enemy.getLocation(), 1);
                                    entMap.setCell(enemy.getLocation(), null);
                                }
                                TowerJunkAttack.getGame().getPlayer().setCash(TowerJunkAttack.getGame().getPlayer().getCash() + 50);
                                PanelManager.RIGHT_BAR.refreshCash();
                                enemiesToMove.remove(i);
                                enemiesListTmp.remove(enemy);
                                i--;
                                continue;
                            }

                            Location nextMove = new Location(enemy.getMobPath().get(enemy.getActualPosOnPath() + 1).x, enemy.getMobPath().get(enemy.getActualPosOnPath() + 1).y);
                            Location oldLocation = enemy.getLocation();
                            int nextMoveDataMapVal = dataMap.getCellData(nextMove);
                            if (nextMoveDataMapVal == 1) {
                                dataMap.setCellData(nextMove, 11);
                                enemy.setLocation(nextMove);
                                entMap.setCell(enemy);
                                enemy.mobMoved();
                                if (dataMap.getCellData(oldLocation) == 11) {
                                    dataMap.setCellData(oldLocation, 1);
                                    entMap.setCell(oldLocation, null);
                                }
                                enemiesToMove.remove(i);
                                i--;
                                continue;
                            }
                            if (dataMap.getCellData(nextMove.getX(), nextMove.getY()) == 3) {
                                if (dataMap.getCellData(oldLocation) == 11) {
                                    dataMap.setCellData(oldLocation, 1);
                                    entMap.setCell(oldLocation, null);
                                }
                                enemiesToMove.remove(i);
                                enemiesListTmp.remove(enemy);
                                i--;
                                Player pl = TowerJunkAttack.getGame().getPlayer();
                                pl.setHp(pl.getHp() - enemy.getHp());
                                PanelManager.RIGHT_BAR.refreshHealth();
                                if (pl.getHp() <= 0) {
                                    JOptionPane.showMessageDialog(null, "Przegrana!");
                                    PanelManager.RIGHT_BAR.setVisible(false);
                                    TowerJunkAttack.getPanel().remove(PanelManager.RIGHT_BAR);
                                    PanelManager.GAME.setVisible(false);
                                    TowerJunkAttack.getPanel().remove(PanelManager.GAME);
                                    PanelManager.MAIN_MENU.setVisible(true);
                                    TowerJunkAttack.getPanel().add(PanelManager.MAIN_MENU);
                                    clearMap();
                                }
                            }
                        }
                    } while (prevSize != enemiesToMove.size());
                }
                enemyList = enemiesListTmp;
                repaint();
                numOfThreadRuning.addAndGet(-1);
            }
        }, 0, updateMobEvery);

        waveTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (game.isPaused()) {
                    return;
                }
                numOfThreadRuning.addAndGet(1);
                if (isVaveCompleted) {
                    waveNum++;
                    Log.info("Starting wave: " + waveNum);

                    for (int i = 0; i < waveNum + 2; i++) {
                        Enemy ene = new Enemy("basic", waveNum + 2, false);
                        nextWave.add(ene);
                    }
                }
                isVaveCompleted = false;
                numOfThreadRuning.addAndGet(-1);
            }
        }, waveStartAfter, waveAfter);

        towerUpdateTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (game.isPaused()) {
                    return;
                }
                List<Tower> towerUpdateList = Collections.synchronizedList(new LinkedList<>(towerList));
                numOfThreadRuning.addAndGet(1);
                int towerToAddSize = towerAddList.size();
                for (int i = 0; i < towerToAddSize; i++) {
                    towerUpdateList.add(towerAddList.get(0));
                    towerAddList.remove(0);
                }

                for (Tower tow : towerUpdateList) {
                    // check for enemies in tower range
                    int x = tow.getLocation().getX();
                    int y = tow.getLocation().getY();
                    double range = tow.getRange();

                    jumpOut:
                    for (int i = x - (int) Math.ceil(range); i < x + (int) Math.ceil(range); ++i) {
                        for (int j = y - (int) Math.ceil(range); j < y + (int) Math.ceil(range); ++j) {
                            if (Math.round(Math.sqrt(Math.pow(y - j, 2) + Math.pow(x - i, 2))) <= range && getCell(i, j).getData() == 11) {
                                Enemy ene = (Enemy) entMap.getCell(i, j);
                                if (ene != null) {
                                    Projectile pro = tow.getProjectile();
                                    if (pro != null) {
                                        pro.setTarget(ene);
                                        projectileListAdd.add(pro);
                                    }
                                    break jumpOut;
                                }
                            }
                        }
                    }
                    // label jump out here
                    //licz++;
                }
                towerList = towerUpdateList;
                numOfThreadRuning.addAndGet(-1);
            }

        }, 0, updateTowerEvery);

        procjectileTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (game.isPaused()) {
                    return;
                }
                numOfThreadRuning.addAndGet(1);
                for (int i = 0; i < projectileListAdd.size(); ++i) {
                    projectileList.add(projectileListAdd.get(i));
                    projectileListAdd.remove(i);
                    i--;
                }

                for (int i = 0; i < projectileList.size(); ++i) {
                    Projectile pro = projectileList.get(i);
                    if (pro.getTarget().getHp() <= 0 && pro.shouldBeDeletedAfterEnemyDies()) {
                        projectileList.remove(i);
                        i--;
                        continue;
                    }

                    // pro.resizeBlock((int)blockSize);
                    pro.update();
                    // remove projectiles       
                    if (pro.isTargetReached()) {
                        if (pro.isDisplayedAferReached()) {
                            projectileList.remove(i);
                            i--;
                        }
                    }
                }
                projectileList = Collections.synchronizedList(new LinkedList<>(projectileList));
                repaint();
                numOfThreadRuning.addAndGet(-1);
            }
        }, 0, updateProjectileEvery);
    }

    public void clearAllTasks() {
        procjectileTimer.cancel();
        towerUpdateTimer.cancel();
        waveTimer.cancel();
        mobUpdateTimer.cancel();
    }

    public List<TowerType> getAvailableTowers() {
        return availableTowers;
    }

    public DataMapCell getCellAt(double xMouse, double yMouse) {
        int x = (int) (xMouse / blockSize);
        int y = (int) (yMouse / blockSize);
        return getCell(x, y);
    }

    public DataMapCell getDataMapCell(Location loc) {
        return dataMap.getCell(loc);
    }

    public void resizeBlock(int size) {
        blockSize = size / dataMap.getMapSize();
    }

    public DataMapCell getCell(int x, int y) {
        return dataMap.getCell(x, y);
    }

    public void setDataMapCell(DataMapCell c) {
        dataMap.setCell(c);
    }

    public void setDataMapTower(DataMapCell c) {
        TowerType tt = TowerType.get(c.getData());
        if (tt != null) {
            Tower tower = new Tower(tt);
            tower.setLocation(new Location(c.getLocation()));
            dataMap.setCell(c);
            dmgMap.setTower(tower);
            entMap.setCell(tower);
            towerAddList.add(tower);
        }
        repaint();
    }

    public LinkedList<Vertex> generatePaths() {
        int dimensionX = dataMap.getSizeX();
        int dimensionY = dataMap.getSizeY();
        Vertex[][] array = new Vertex[dimensionX][dimensionY];
        Stack<Vertex> stack = new Stack<>();
        LinkedList<Vertex> list = new LinkedList<>();

        PriorityQueue<Vertex> queue = new PriorityQueue<>(dimensionX * dimensionY, (v1, v2) -> v1.costOfGettingHere - v2.costOfGettingHere);

        for (int i = 0; i < dimensionX; i++) {
            for (int j = 0; j < dimensionY; j++) {
                if (dataMap.getCellData(i, j) != 0) {
                    array[i][j] = new Vertex(i, j, (int) dmgMap.getCell(i, j));
                } else {
                    array[i][j] = new Vertex(i, j, Integer.MAX_VALUE);
                }
            }
        }
        queue.add(array[endPoint.getX()][endPoint.getY()]);
        while (!queue.isEmpty()) {
            Vertex curr = queue.poll();
            stack.push(curr);
            List<Vertex> vertices = new ArrayList<>();
            if (curr.y - 1 >= 0) {
                vertices.add(array[curr.x][curr.y - 1]);
            }
            if (curr.y + 1 < dimensionY) {
                vertices.add(array[curr.x][curr.y + 1]);
            }
            if (curr.x - 1 >= 0) {
                vertices.add(array[curr.x - 1][curr.y]);
            }
            if (curr.x + 1 < dimensionX) {
                vertices.add(array[curr.x + 1][curr.y]);
            }
            for (Vertex v : vertices) {
                if (v.costOfEnteringThisOne < 5555) {
                    if (v.costOfGettingHere > curr.costOfGettingHere + v.costOfEnteringThisOne) {
                        v.costOfGettingHere = curr.costOfGettingHere + v.costOfEnteringThisOne;
                        queue.add(v);
                    }
                }
            }
        }
        Vertex finish = new Vertex(startPoint.getX(), startPoint.getY(), 2478921);
        for (Vertex vertex : stack) {
            if (finish.x == vertex.x && finish.y == vertex.y) {
                finish = vertex;
            }
        }
        list.addLast(finish);
        Vertex curr = finish;
        while (!stack.isEmpty()) {
            if ((curr.costOfGettingHere < list.getLast().costOfGettingHere) && ((Math.abs(curr.x - list.getLast().x) == 1) && (curr.y - list.getLast().y == 0)) || (Math.abs(curr.y - list.getLast().y) == 1 && (curr.x - list.getLast().x == 0))) {
                list.addLast(curr);
            }
            curr = stack.pop();
        }
        list.add(new Vertex(endPoint.getX(), endPoint.getY(), 0));
        return list;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

    public int getBlockSize() {
        return blockSize;
    }

    @Override
    public synchronized void paint(Graphics g) {
        for (int i = 0; i < dataMap.getSizeX(); i++) {
            for (int j = 0; j < dataMap.getSizeY(); j++) {
                switch (dataMap.getCellData(i, j)) {
                    case 0:
                        g.setColor(Color.BLACK);
                        break;
                    case 1:
                        g.setColor(Color.WHITE);
                        break;
                    case 2:
                        g.setColor(Color.RED);
                        break;
                    case 3:
                        g.setColor(Color.GREEN);
                        break;
                    case 11:
                        g.setColor(Color.WHITE); // To clear map from projectile mess
                        break;
                    default:
                        g.setColor(Color.GRAY);
                        break;
                }
                g.fillRect(j * blockSize, i * blockSize, blockSize, blockSize);
            }
        }

        // TODO: Change this to some kind reference switching rather than creating new one
        // See projectile print list for reference
        List<Enemy> enemyPrintList = enemyList;

        enemyPrintList.forEach((enemy) -> {
            int i = enemy.getLocation().getX();
            int j = enemy.getLocation().getY();
            int color = (int) (enemy.getHp() * 255.0 / enemy.getMaxHp());
            g.setColor(new Color(255 - color, color, 0));
            g.fillPolygon(createPolygon(enemy.getVertexNumber(), new Rectangle(j * blockSize, i * blockSize, blockSize, blockSize)));
        });

        for (int k = 0; k < towerList.size(); ++k) {
            Tower t = towerList.get(k);
            if (t.getImage() != null) {
                int i = t.getLocation().getX();
                int j = t.getLocation().getY();
                g.drawImage(scale(t.getImage(), TowerJunkAttack.getSize() / dataMap.getMapSize(), TowerJunkAttack.getSize() / dataMap.getMapSize()), j * blockSize, i * blockSize, this);
            }
        }

        List<Projectile> projectilePrintList = projectileList;

        projectilePrintList.forEach((pro) -> {
            pro.render(g, blockSize);
        });
    }

    public static BufferedImage scale(BufferedImage imageToScale, int dWidth, int dHeight) {
        BufferedImage scaledImage = null;
        if (imageToScale != null) {
            scaledImage = new BufferedImage(dWidth, dHeight, imageToScale.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();

            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
            graphics2D.dispose();
        }
        return scaledImage;
    }

    public static Polygon createPolygon(int vertices, Rectangle r) {
        Polygon polygon = new Polygon();
        int x = (r.width >> 1) + r.x;
        int y = (r.height >> 1) + r.y;
        int radius = (int) (Math.min(r.width, r.height) * 0.45);
        double angle = 2 * Math.PI / vertices;
        for (int i = 0; i < vertices; i++) {
            int p1 = (int) (x + radius * Math.cos(i * angle));
            int p2 = (int) (y - radius * Math.sin(i * angle));
            polygon.addPoint(p1, p2);
        }
        return polygon;
    }

    @Override
    public String getName() {
        return name;
    }

    public List<Tower> getTowers() {
        return towerList;
    }

    public void clearMap() {
        clearAllTasks();
        while (numOfThreadRuning.get() > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }
        }

        waveNum = 0;
        nextWave.clear();
        enemyList.clear();
        towerList.clear();
        projectileList.clear();
        projectileListAdd.clear();
        availableTowers.clear();
    }

}
