/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tja.map;

import net.tja.Location;
import net.tja.tower.Tower;

public class DamageMap {

    private float[][] map;
    private int sizeX, sizeY;
    private DataMap dataMap;

    public DamageMap(int sizeX, int sizeY, DataMap dataMap) {
        map = new float[sizeX][sizeY];
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.dataMap = dataMap;
        fillMapDmg();
    }

    private void fillMapDmg() {
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                int mapaCellVal = dataMap.getCellData(i, j);
                if (mapaCellVal == 1 || mapaCellVal == 2 || mapaCellVal == 3) {
                    map[i][j] = 1;
                } else {
                    map[i][j] = Float.MAX_VALUE;
                }
            }
        }
    }

    public void setTower(Tower tower) {
        int tx = tower.getLocation().getX();
        int ty = tower.getLocation().getY();
        double range = tower.getRange();
        double damage = tower.getDamage();
        int left = Math.max(0, (int) Math.round(tx - range));
        int right = Math.min(sizeX - 1, (int) Math.round(tx + range));
        int top = Math.max(0, (int) Math.round(ty - range));
        int down = Math.min(sizeY - 1, (int) Math.round(ty + range));
        for (int i = left; i <= right; i++) {
            for (int j = top; j <= down; j++) {
                if (Math.round(Math.sqrt(Math.pow(ty - j, 2) + Math.pow(tx - i, 2))) <= range && map[i][j] != Float.MAX_VALUE) {
                    map[i][j] += damage;
                }
            }
        }
    }

    public float getCell(Location loc) {
        return getCell(loc.getX(), loc.getY());
    }

    public float getCell(int x, int y) {
        if (x >= 0 && x < sizeX && y >= 0 && y < sizeY) {
            return map[x][y];
        } else {
            return 0;
        }
    }

}
