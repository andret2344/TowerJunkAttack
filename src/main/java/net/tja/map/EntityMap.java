/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tja.map;

import lombok.Getter;
import net.tja.Entity;
import net.tja.Location;

public class EntityMap {
    private final Entity[][] map;
    @Getter
    private final int sizeX;
    @Getter
    private final int sizeY;

    public EntityMap(int sizeX, int sizeY) {
        map = new Entity[sizeX][sizeY];
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public synchronized Entity getCell(int x, int y) {
        return map[x][y];
    }

    public synchronized Entity getCell(Location loc) {
        return map[loc.getX()][loc.getY()];
    }

    public void setCell(Entity ent) {
        setCell(ent.getLocation(), ent);
    }

    public synchronized void setCell(Location loc, Entity ent) {
        map[loc.getX()][loc.getY()] = ent;
    }
}
