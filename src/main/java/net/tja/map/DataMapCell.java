/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tja.map;

import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import net.tja.Location;

@Value
public class DataMapCell {

    private final Location location;
    @Setter
    @NonFinal
    private int data;

    public DataMapCell(int x, int y) {
        this(new Location(x, y));
    }

    public DataMapCell(Location location) {
        this.location = location;
    }

    public DataMapCell(DataMapCell cell) {
        location = cell.location;
        data = cell.data;
    }
}
