package net.tja;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum OperatingSystem {
    WINDOWS(18, 45),
    LINUX(0, 0),
    MAC(0, 20);

    @Getter
    private final int borderSizeX, borderSizeY;
}