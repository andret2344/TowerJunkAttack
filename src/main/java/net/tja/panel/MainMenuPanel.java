package net.tja.panel;

import net.tja.TowerJunkAttack;

import javax.swing.*;
import java.awt.*;

public class MainMenuPanel extends JPanel {
    public MainMenuPanel() {
        //start
        JButton b = new JButton("Start Game");
        b.setSize(new Dimension(200, 50));
        b.setLocation(100, 100);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(b);
        b.addActionListener(e -> {
            PanelManager.MAIN_MENU.setVisible(false);
            TowerJunkAttack.getPanel().remove(PanelManager.MAIN_MENU);
            PanelManager.GAME.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.GAME);
            PanelManager.RIGHT_BAR.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.RIGHT_BAR);
            PanelManager.RIGHT_BAR.setBackground(Color.white);
            PanelManager.RIGHT_BAR.setBounds(0, 500, 100, 500);
            TowerJunkAttack.getGame().startGame();
        });
        //history
        JButton historyButton = new JButton("Show History");
        historyButton.setSize(new Dimension(200, 50));
        historyButton.setLocation(b.getVerticalAlignment() + 100, b.getHorizontalAlignment());
        add(historyButton);
        historyButton.addActionListener(e -> {
            PanelManager.MAIN_MENU.setVisible(false);
            TowerJunkAttack.getPanel().remove(PanelManager.MAIN_MENU);
            PanelManager.HISTORY.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.HISTORY);
        });
        //authors
        JButton authorsButton = new JButton("Show Authors");
        authorsButton.setSize(new Dimension(200, 50));
        authorsButton.setLocation(b.getVerticalAlignment() + 200, b.getHorizontalAlignment());
        add(authorsButton);
        authorsButton.addActionListener(e -> {
            PanelManager.MAIN_MENU.setVisible(false);
            TowerJunkAttack.getPanel().remove(PanelManager.MAIN_MENU);
            PanelManager.AUTHORS.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.AUTHORS);
        });
        //tutorial
        JButton tutorialButton = new JButton("Show Tutorial");
        tutorialButton.setSize(new Dimension(200, 50));
        tutorialButton.setLocation(b.getVerticalAlignment() + 300, b.getHorizontalAlignment());
        add(tutorialButton);
        tutorialButton.addActionListener(e -> {
            PanelManager.MAIN_MENU.setVisible(false);
            TowerJunkAttack.getPanel().remove(PanelManager.MAIN_MENU);
            PanelManager.TUTORIAL.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.TUTORIAL);
        });
    }
}