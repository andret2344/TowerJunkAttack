package net.tja.panel;

import net.tja.TowerJunkAttack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TutorialPanel extends JPanel {

    TutorialPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        //menu button
        JButton menuButton = new JButton("MENU");
        menuButton.setSize(new Dimension(200, 50));
        menuButton.setLocation(100, 100);
        add(menuButton);
        String str = "Witaj w tutorialu!\nTutaj dowiesz się jak grać!\n\n\n\n\nKiedys...";
        str = "<html>" + str.replace("\n", "<br>") + "</html>";
        JLabel text = new JLabel(str);
        text.setFont(new Font(text.getFont().getName(), Font.PLAIN, 20));
        text.setPreferredSize(null);
        text.setAlignmentX(CENTER_ALIGNMENT);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        add(text);

        menuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelManager.MAIN_MENU.setVisible(true);
                TowerJunkAttack.getPanel().add(PanelManager.MAIN_MENU);
                PanelManager.TUTORIAL.setVisible(false);
                TowerJunkAttack.getPanel().remove(PanelManager.TUTORIAL);
                PanelManager.MAIN_MENU.revalidate();
                PanelManager.MAIN_MENU.repaint();
            }
        });
    }

}
