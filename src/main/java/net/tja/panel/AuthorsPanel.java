package net.tja.panel;

import net.tja.TowerJunkAttack;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class AuthorsPanel extends JPanel {

    AuthorsPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        //menu button
        JButton menuButton = new JButton("MENU");
        menuButton.setSize(new Dimension(200, 50));
        menuButton.setLocation(100, 100);
        add(menuButton);
        List<String> authors = Arrays.asList("Chmiel Andrzej", "Czernecki Maksymilian", "Slesarczyk Radosław", "Zadara Maciej");
        StringBuilder str = new StringBuilder();
        for (String s : authors) {
            str.append("<br>").append(s);
        }
        str = new StringBuilder("<html>" + "Programming and assets:" + str + "<br>Music:<br> MPC Project Music Entertainment LLC</html>");
        JLabel text = new JLabel(str.toString());
        text.setFont(new Font(text.getFont().getName(), Font.PLAIN, 20));
        text.setPreferredSize(null);
        text.setAlignmentX(CENTER_ALIGNMENT);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        add(text);

        menuButton.addActionListener(e -> {
            PanelManager.MAIN_MENU.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.MAIN_MENU);
            PanelManager.AUTHORS.setVisible(false);
            TowerJunkAttack.getPanel().remove(PanelManager.AUTHORS);
            PanelManager.MAIN_MENU.revalidate();
            PanelManager.MAIN_MENU.repaint();
        });
    }

}
