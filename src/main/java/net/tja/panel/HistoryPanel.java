package net.tja.panel;

import net.tja.History;
import net.tja.TowerJunkAttack;

import javax.swing.*;
import java.awt.*;

public class HistoryPanel extends JPanel {

    HistoryPanel() {
        History history = TowerJunkAttack.getHistory();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        //menu button
        JButton menuButton = new JButton("MENU");
        menuButton.setSize(new Dimension(200, 50));
        menuButton.setLocation(100, 100);
        add(menuButton);
        String str = "<html>" + history.toString().replaceAll(";", "<br>").replaceAll(",", " ") + "</html>";
        JLabel text = new JLabel(str);
        text.setFont(new Font(text.getFont().getName(), Font.PLAIN, 20));
        text.setPreferredSize(null);
        text.setAlignmentX(CENTER_ALIGNMENT);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        add(text);

        menuButton.addActionListener(e -> {
            PanelManager.MAIN_MENU.setVisible(true);
            TowerJunkAttack.getPanel().add(PanelManager.MAIN_MENU);
            PanelManager.HISTORY.setVisible(false);
            TowerJunkAttack.getPanel().remove(PanelManager.HISTORY);
            PanelManager.MAIN_MENU.revalidate();
            PanelManager.MAIN_MENU.repaint();
        });
    }

}
