package net.tja.panel;

import net.tja.Game;
import net.tja.TowerJunkAttack;
import net.tja.tower.Tower;
import net.tja.tower.TowerType;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

public class RightBarPanel extends JPanel {

    private final static ImageIcon IconVol0 = new ImageIcon("assets" + File.separator + "volume_0.png");
    private final static ImageIcon IconVol1 = new ImageIcon("assets" + File.separator + "volume_1.png");
    private final static ImageIcon IconVol2 = new ImageIcon("assets" + File.separator + "volume_2.png");
    private final static ImageIcon IconVol3 = new ImageIcon("assets" + File.separator + "volume_3.png");
    private static int lastVolume;
    private static Icon lastIcon;
    private JLabel pointsLabel, cashLabel, towerInfo = new JLabel(""), healthLabel, nameLabel;
    private final Game game;

    public RightBarPanel(Game game) {
        this.game = game;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel soundLabel = new JLabel(IconVol3);
        soundLabel.setPreferredSize(null);
        soundLabel.setSize(new Dimension(96, 96));
        soundLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(soundLabel);
        //slider
        JSlider volumeSlider = new JSlider();
        volumeSlider.setPreferredSize(new Dimension(0, 0));
        volumeSlider.setMaximumSize(new Dimension(100, 50));
        volumeSlider.setValue(100);
        volumeSlider.setMinimum(100);
        volumeSlider.setMinimum(0);
        add(volumeSlider);
        //player info
        nameLabel = new JLabel();
        nameLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(nameLabel);
        //points
        pointsLabel = new JLabel();
        pointsLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(pointsLabel);
        //health
        healthLabel = new JLabel();
        healthLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(healthLabel);

        cashLabel = new JLabel();
        cashLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(cashLabel);
        //tower info
        JLabel towerLabel = new JLabel("Available towers:");
        towerLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(towerLabel);
        for (TowerType tt : game.getMap().getAvailableTowers()) {
            Tower tower = new Tower(tt);
            ImageIcon towerIcon = new ImageIcon("assets" + File.separator + tower.getType().getFilename() + ".png");
            JLabel availableTowersLabel = new JLabel("", towerIcon, SwingConstants.CENTER);
            availableTowersLabel.setOpaque(true);
            availableTowersLabel.setBackground(Color.white);
            availableTowersLabel.setAlignmentX(CENTER_ALIGNMENT);
            towerInfo.setMaximumSize(new Dimension(100, 150));
            towerInfo.setAlignmentX(CENTER_ALIGNMENT);
            availableTowersLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    towerInfo.setText("<html>" + tower.getName() +
                            "<br>Price: " + tower.getPrice() +
                            "<br>Damage: " + tower.getDamage() +
                            "<br>Range: " + tower.getRange() +
                            "<br>Delay: " + tower.getDelay() +
                            "</html>");
                    add(towerInfo);
                    game.setSelectedTower(tower.getType());
                }
            });
            add(availableTowersLabel);
            add(towerInfo);
        }

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            if (e.getID() != KeyEvent.KEY_RELEASED) {
                return false;
            }
            Tower tower;
            switch (e.getKeyChar()) {
                case '1':
                    tower = new Tower(game.getMap().getAvailableTowers().get(0));
                    break;
                case '2':
                    tower = new Tower(game.getMap().getAvailableTowers().get(1));
                    break;
                case '3':
                    tower = new Tower(game.getMap().getAvailableTowers().get(2));
                    break;
                case '4':
                    tower = new Tower(game.getMap().getAvailableTowers().get(3));
                    break;
                default:
                    return false;
            }
            towerInfo.setText("<html>" + tower.getName() +
                    "<br>Price: " + tower.getPrice() +
                    "<br>Damage: " + tower.getDamage() +
                    "<br>Range: " + tower.getRange() +
                    "<br>Delay: " + tower.getDelay() +
                    "</html>");
            add(towerInfo);
            game.setSelectedTower(tower.getType());
            return true;
        });

        soundLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (TowerJunkAttack.getSoundVolume() != 0) {
                    lastVolume = TowerJunkAttack.getSoundVolume();
                    lastIcon = soundLabel.getIcon();
                    TowerJunkAttack.setSoundVolume(0);
                    soundLabel.setIcon(IconVol0);
                } else {
                    TowerJunkAttack.setSoundVolume(lastVolume);
                    soundLabel.setIcon(lastIcon);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        volumeSlider.addChangeListener(new SliderListener(volumeSlider, soundLabel));

    }

    private class SliderListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider) e.getSource();
            if (!source.getValueIsAdjusting()) {
                int volume = (int) source.getValue();
                TowerJunkAttack.setSoundVolume(volume);
                switch (volume / 25) {
                    case 0:
                        label.setIcon(IconVol0);
                        break;
                    case 1:
                        label.setIcon(IconVol1);
                        break;
                    case 2:
                        label.setIcon(IconVol2);
                        break;
                    case 3:
                        label.setIcon(IconVol3);
                        break;
                }
            }
        }

        public SliderListener(JSlider slider, JLabel label) {
            this.slider = slider;
            this.label = label;
        }

        private final JSlider slider;
        private final JLabel label;
    }

    public void refreshPoints() {
        pointsLabel.setText("Punkty: " + game.getPlayer().getPoints());
    }

    public void refreshHealth() {
        healthLabel.setText("Zycie: " + game.getPlayer().getHp());
    }

    public void refreshCash() {
        cashLabel.setText("Money: " + game.getPlayer().getCash());
    }

    public void refreshPlayerInfo() {
        nameLabel.setText(game.getPlayer().getName());
    }

    public void removeTowerInfo() {
        towerInfo.setText("<html><br><br><br><br></html>");
    }
}
