package net.tja.panel;

import net.tja.TowerJunkAttack;

public final class PanelManager {
    public static final GamePanel GAME = new GamePanel(TowerJunkAttack.getGame());
    public static final MainMenuPanel MAIN_MENU = new MainMenuPanel();
    public static final RightBarPanel RIGHT_BAR = new RightBarPanel(TowerJunkAttack.getGame());
    public static final HistoryPanel HISTORY = new HistoryPanel();
    public static final AuthorsPanel AUTHORS = new AuthorsPanel();
    public static final TutorialPanel TUTORIAL = new TutorialPanel();
    public static final LoginPanel LOGIN = new LoginPanel();
    // public static final TutorialPanel TUTORIAL = new TutorialPanel();

    private PanelManager() {
    }
}