package net.tja.panel;

import lombok.Getter;
import net.tja.Game;
import net.tja.TowerJunkAttack;

import javax.swing.*;

public class GamePanel extends JPanel {
    @Getter
    private Game game;

    public GamePanel(Game game) {
        this.game = game;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setSize(TowerJunkAttack.getSize(), TowerJunkAttack.getSize());
        add(game.getMap());
    }
}