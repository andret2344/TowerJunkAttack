package net.tja.panel;

import net.tja.Player;
import net.tja.TowerJunkAttack;

import javax.swing.*;
import java.sql.*;

public class LoginPanel extends JPanel {
    public LoginPanel() {
        //start
        setLayout(null);

        JLabel loginLabel = new JLabel("Login: ");
        loginLabel.setBounds(10, 10, 80, 25);
        add(loginLabel);

        JTextField loginText = new JTextField(20);
        loginText.setBounds(100, 10, 160, 25);
        add(loginText);

        JLabel passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(10, 40, 80, 25);
        add(passwordLabel);

        JPasswordField passwordText = new JPasswordField(20);
        passwordText.setBounds(100, 40, 160, 25);
        add(passwordText);

        JButton loginButton = new JButton("Login!");
        loginButton.setBounds(10, 80, 80, 25);
        add(loginButton);

        loginButton.addActionListener(e -> {
            try {
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/tja?autoReconnect=true&useSSL=false", "root", "root");
                Statement stat = conn.createStatement();
                stat.execute("USE tja");
                PreparedStatement pstat = conn.prepareStatement("SELECT password FROM users WHERE `username`=?");
                pstat.setString(1, loginText.getText());
                ResultSet rs = pstat.executeQuery();
                if (rs.next()) {
                    if (rs.getString(1).equals(new String(passwordText.getPassword()))) {
                        PanelManager.LOGIN.setVisible(false);
                        TowerJunkAttack.getPanel().remove(PanelManager.LOGIN);
                        PanelManager.MAIN_MENU.setVisible(true);
                        TowerJunkAttack.getGame().setPlayer(new Player(loginText.getText()));
                        PanelManager.RIGHT_BAR.refreshPlayerInfo();
                        PanelManager.RIGHT_BAR.refreshHealth();
                        PanelManager.RIGHT_BAR.refreshCash();
                        TowerJunkAttack.getPanel().add(PanelManager.MAIN_MENU);
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });

        JButton registerButton = new JButton("Register");
        registerButton.setBounds(180, 80, 80, 25);
        add(registerButton);
    }
}