package net.tja.tower;

import net.tja.DownloadableContent;
import net.tja.Entity;
import net.tja.projectile.Projectile;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.image.BufferedImage;

public class Tower extends Entity {
    private double damage;
    private double range;
    private double projectileSpeed;
    private int price;
    private int delay;
    private BufferedImage image;
    private TowerType type;
    private DownloadableContent dlc;
    public Class<? extends Projectile> projectileType;

    public Tower(TowerType type) {
        super(type.getName());
        this.type = type;
        damage = type.getDamage();
        range = type.getRange();
        price = type.getPrice();
        delay = type.getDelay();
        image = type.getImage();
        projectileSpeed = type.getProjectileSpeed();
        dlc = type.getDLCNumber();
        projectileType = type.getProjectileType();
    }

    public TowerType getType() {
        return type;
    }

    public double getDamage() {
        return damage;
    }

    public double getRange() {
        return range;
    }

    public int getPrice() {
        return price;
    }

    public int getDelay() {
        return delay;
    }

    public BufferedImage getImage() {
        return image;
    }

    public DownloadableContent getDLCNumber() {
        return dlc;
    }

    public boolean update(TowerType type) {
        if (!type.isParentOf(type)) {
            return false;
        }
        super.name = type.getName();
        this.type = type;
        damage = type.getDamage();
        range = type.getRange();
        price = type.getPrice();
        delay = type.getDelay();
        image = type.getImage();
        return true;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("type", type.name());
        json.put("x", location.getX());
        json.put("y", location.getY());
        return json;
    }

    public Projectile getProjectile() {
        try {
            Projectile pro = projectileType.newInstance();
            pro.setSource(this);
            pro.setDamage(damage);
            pro.setMaxSpeed(projectileSpeed);
            return pro;
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println(name + "have not exisiting projectiles");
        }
        return null;
    }

    @Override
    public String toString() {
        return "Tower [type=" + type + ", damage=" + damage + ", range=" + range + ", price=" + price + ", delay=" + delay + "]";
    }
}
