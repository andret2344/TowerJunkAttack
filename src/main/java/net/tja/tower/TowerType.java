package net.tja.tower;

import net.tja.DownloadableContent;
import net.tja.projectile.Laser;
import net.tja.projectile.Projectile;
import net.tja.projectile.Rocket;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public enum TowerType {

    BASE(101, "base_tower", "Base tower", 10, 10, 100, 500, 100, DownloadableContent.BASE, Laser.class),
    QUICK(102, "quick_tower", "Quick tower", 10, 10, 350, 200, 100, DownloadableContent.BASE, Laser.class),
    STRONG(103, "strong_tower", "Strong tower", 30, 10, 400, 500, 2, DownloadableContent.BASE, Rocket.class),
    WIDE(104, "wide_tower", "Strong tower", 10, 30, 700, 500, 100, DownloadableContent.BASE, Laser.class);

    private final int id;
    private final String filename;
    private final String towerName;
    private final double damage;
    private final double range;
    private final int price;
    private final int delay;
    private final double projectileSpeed;
    private final DownloadableContent dlc;
    private BufferedImage image;
    private final List<TowerType> children = new ArrayList<>();
    public Class<? extends Projectile> projectileType;

    TowerType(int id, String filename, String towerName, double damage, double range,
              int price, int delay, double proSpeed, DownloadableContent dlc, Class<? extends Projectile> projectileType) {
        this.id = id;
        this.filename = filename;
        this.towerName = towerName;
        this.damage = damage;
        this.range = range;
        this.price = price;
        this.delay = delay;
        projectileSpeed = proSpeed;
        this.dlc = dlc;
        this.projectileType = projectileType;
        try {
            image = ImageIO.read(new File("assets", filename + ".png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    BufferedImage getImage() {
        return image;
    }

    String getName() {
        return towerName;
    }

    double getDamage() {
        return damage;
    }

    double getRange() {
        return range;
    }

    int getPrice() {
        return price;
    }

    double getProjectileSpeed() {
        return projectileSpeed;
    }

    Class<? extends Projectile> getProjectileType() {
        return projectileType;
    }

    int getDelay() {
        return delay;
    }

    DownloadableContent getDLCNumber() {
        return dlc;
    }

    public boolean isParentOf(TowerType type) {
        return children.contains(type);
    }

    public static TowerType get(int id) {
        for (TowerType t : values()) {
            if (t.id == id) {
                return t;
            }
        }
        return null;
    }
}
