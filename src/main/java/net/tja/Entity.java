package net.tja;

import lombok.Data;

@Data
public abstract class Entity {
    protected String name;
    protected Location location;

    public Entity(String name) {
        this.name = name;
    }
}
