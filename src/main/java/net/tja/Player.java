package net.tja;

import lombok.Data;

@Data
public class Player {
    private final String name;
    private int hp = 1000;
    private float cash = 1_000;
    private int points = 0;
    private int maxHp = 100;

    public Player(String name) {
        this.name = name;
    }
}
