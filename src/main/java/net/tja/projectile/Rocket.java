package net.tja.projectile;

import net.tja.Enemy;
import net.tja.tower.Tower;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Go for target. If target dies it gets destoyed
 *
 * @author Pandemon
 */
public class Rocket extends Projectile {

    public Rocket(Tower source, Enemy target) {
        this(source, target, 100, 100);
    }

    public Rocket(Tower source, Enemy target, double damage) {
        this(source, target, damage, 100);
    }

    public Rocket(Tower source, Enemy target, double damage, double maxSpeed) {
        super(source, target, damage, maxSpeed);
        deleteAfterEnemyLost = false;
    }

    @Override
    public void update() {
        if (targetReached) {
            return;
        }
        int halfBloc = prevBlockSize / 2;
        vector = new Projectile.Point(target.getLocation().getX() * prevBlockSize + halfBloc - location.x,
                target.getLocation().getY() * prevBlockSize + halfBloc - location.y);
        double length = vector.getLenght();
        Projectile.Point newLoc;
        if (length > maxSpeed) {
            double scale = length / maxSpeed;
            vector.div(scale);
            newLoc = new Projectile.Point(location.x + vector.x, location.y + vector.y);
        } else {
            newLoc = new Projectile.Point(target.getLocation());
            newLoc.mult(prevBlockSize);
            newLoc.add(prevBlockSize / 2);
            target.setHp(target.getHp() - (int) damage);
            targetReached = true;
        }
        prevLocation = location;
        location = newLoc;
    }

    @Override
    public void render(Graphics g, int blockSize) {
        super.render(g, blockSize);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.RED);
        g2.setStroke(new BasicStroke(2));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.draw(new Line2D.Double(location.y, location.x, prevLocation.y, prevLocation.x));
    }
}
