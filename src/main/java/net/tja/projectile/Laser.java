package net.tja.projectile;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import net.tja.tower.Tower;
import net.tja.Enemy;

/**
 * Go for target. If target dies it gets destoyed
 * @author Pandemon
 */
public class Laser extends Projectile{
    
        public Laser(){
        }
    
	public Laser(Tower source, Enemy target) {
		this(source, target, 10, 30);
	}
	
	public Laser(Tower source, Enemy target, double damage) {
		this(source, target,damage, 1000);
	}
	
	public Laser(Tower source, Enemy target, double damage, double maxSpeed) {
		super(source, target, damage, maxSpeed);			
		deleteAfterEnemyLost = true;
	}

	@Override
	public void update(){
		if (targetReached == true) {
			return;
		}
		int halfBloc = prevBlockSize/2;
		vector = new Point(	target.getLocation().getX()*prevBlockSize + halfBloc - location.x, 
							target.getLocation().getY()*prevBlockSize + halfBloc - location.y);
		double length = vector.getLenght();
		Point newLoc;
		if (length > maxSpeed) {
			double scale = length / maxSpeed;
			vector.div(scale);
			newLoc = new Point(location.x + vector.x, location.y + vector.y);
		} else {
			newLoc = new Point(target.getLocation());
			newLoc.mult(prevBlockSize);
			newLoc.add(prevBlockSize/2);
			target.setHp(target.getHp() - (int)damage);
			targetReached = true;
		}
		this.prevLocation = this.location;
		this.location = newLoc;
	}
	
	@Override
	public void render(Graphics g, int blockSize){
		super.render(g, blockSize);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.BLUE);
		g2.setStroke(new BasicStroke(2));
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.draw(new Line2D.Double(location.y, location.x, prevLocation.y, prevLocation.x));  
	}
}
