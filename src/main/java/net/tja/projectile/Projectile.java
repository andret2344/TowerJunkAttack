package net.tja.projectile;

import lombok.AllArgsConstructor;
import net.tja.Enemy;
import net.tja.Entity;
import net.tja.Location;
import net.tja.TowerJunkAttack;
import net.tja.tower.Tower;

import java.awt.*;

public abstract class Projectile extends Entity {
    protected boolean targetReached = false;
    protected boolean displayedAfterReached = false;
    protected Enemy target;
    protected Point vector;
    protected double delay = 100;
    // We need better precision location
    protected Point location;
    protected Point prevLocation;
    protected int prevBlockSize = 1;
    protected Tower source;
    // default setings for projectiles
    protected double damage = 100;
    protected double maxSpeed = 20;
    protected boolean deleteAfterEnemyLost = false;

    @AllArgsConstructor
    public class Point {
        public double x, y;

        public Point(Location loc) {
            this(loc.getX(), loc.getY());
        }

        public double getLenght() {
            return Math.sqrt(x * x + y * y);
        }

        public Point() {
            this(0, 0);
        }

        void sub(Point p2) {
            x -= p2.x;
            y -= p2.y;
        }

        void sub(double val) {
            x -= val;
            y -= val;
        }

        void add(Point p2) {
            x += p2.x;
            y += p2.y;
        }

        void add(double val) {
            x += val;
            y += val;
        }

        void div(double sc) {
            x /= sc;
            y /= sc;
        }

        void mult(double sc) {
            x *= sc;
            y *= sc;
        }
    }

    public Projectile() {
        super("Projectile");
    }

    public Projectile(Tower source, Enemy target) {
        super("Projectile");
        this.source = source;
        setSourceAndLocation(source);
    }

    public Projectile(Tower source, Enemy target, double damage) {
        this(source, target);
        this.damage = damage;
    }

    public Projectile(Tower source, Enemy target, double damage, double maxSpeed) {
        this(source, target);
        this.damage = damage;
        this.maxSpeed = maxSpeed;
        this.maxSpeed = maxSpeed * prevBlockSize;
    }

    public abstract void update();

    public void setTarget(Enemy enemy) {
        target = enemy;
    }

    private void setSourceAndLocation(Tower tower) {
        source = tower;
        int size = TowerJunkAttack.getGame().getMap().getBlockSize();
        prevBlockSize = size;
        int halfBlock = size / 2;
        location = new Point(source.getLocation().getX() * size + halfBlock,
                source.getLocation().getY() * size + halfBlock);
        prevLocation = location;
    }

    public void setSource(Tower tower) {
        if (source == null) {
            setSourceAndLocation(tower);
        }
    }

    public boolean shouldBeDeletedAfterEnemyDies() {
        return deleteAfterEnemyLost;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed * prevBlockSize;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public boolean isTargetReached() {
        return targetReached;
    }

    public boolean isDisplayedAferReached() {
        return displayedAfterReached;
    }

    public void render(Graphics g, int blockSize) {
        //If map resized
        resizeBlock(blockSize);
        if (targetReached) {
            displayedAfterReached = true;
        }
    }

    public Enemy getTarget() {
        return target;
    }

    public int getBlockSize() {
        return prevBlockSize;
    }

    public Tower getSource() {
        return source;
    }

    public void resizeBlock(int newBlockSize) {
        if (newBlockSize == prevBlockSize) {
            return;
        }
        double scale = (newBlockSize * 1.0) / prevBlockSize;
        location.mult(scale);
        maxSpeed = maxSpeed * scale;
        prevLocation.mult(scale);
        prevBlockSize = newBlockSize;
    }
}
