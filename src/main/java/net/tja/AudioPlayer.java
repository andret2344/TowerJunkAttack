package net.tja;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * An utility class for playing back audio files using Java Sound API.
 *
 * @author www.codejava.net
 *
 */
public class AudioPlayer implements LineListener {

	private boolean playCompleted;
	private boolean isStopped;
	private boolean isPaused;
	private boolean isLooped;
	private Clip audioClip;

	public void loadFile(String audioFilePath) throws UnsupportedAudioFileException, LineUnavailableException, IOException {
		File audioFile = new File(audioFilePath);
		AudioInputStream audioStream;
		audioStream = AudioSystem.getAudioInputStream(audioFile);
		AudioFormat format = audioStream.getFormat();
		DataLine.Info info = new DataLine.Info(Clip.class, format);
		audioClip = (Clip) AudioSystem.getLine(info);
		audioClip.addLineListener(this);
		audioClip.open(audioStream);
	}

	@SuppressWarnings({"SleepWhileInLoop", "CallToPrintStackTrace"})
	void play() {
		audioClip.start();

		if(isLooped)
			audioClip.loop(Clip.LOOP_CONTINUOUSLY);
		
		playCompleted = false;
		isStopped = false;

		while (!playCompleted) {
			// wait for the playback completes
			try {
				Thread.sleep(200);
			} catch (InterruptedException ex) {
				if (isStopped) {
					audioClip.stop();
					break;
				}
				if (isPaused) {
					audioClip.stop();
				} else {
					audioClip.start();
				}
			}
		}

		audioClip.close();
	}

	public void stop() {
		isStopped = true;
	}

	public void pause() {
		isPaused = true;
	}
	
	public void loop(){
		isLooped = true;
	}
	
	public void stopLoop(){
		isLooped = false;
	}

	public void resume() {
		isPaused = false;
	}

	@Override
	public void update(LineEvent event) {
		LineEvent.Type type = event.getType();
		if (type == LineEvent.Type.STOP) {
			if (isStopped || !isPaused) {
					playCompleted = true;
			}
		}
	}

	public Clip getAudioClip() {
		return audioClip;
	}
}
