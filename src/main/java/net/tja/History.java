package net.tja;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.time.LocalDate;

/**
 *
 * @author Radek
 */
public class History {
	private static LinkedList<HistoryEntry> history = new LinkedList<>();

	public class HistoryEntry {
		private String name;
		private int points;
		private LocalDate date;

		HistoryEntry() {
			this("TestName", 0, LocalDate.now());
		}
		
		HistoryEntry(String[] elements) {
			this(elements[0], Integer.parseInt(elements[1]), LocalDate.parse(elements[2].substring(0, 10)));
		}
		
		HistoryEntry(Game game) {
			this(game.getPlayer().getName(), game.getPlayer().getPoints(), LocalDate.now());
		}

		HistoryEntry(String name, int points, LocalDate date) {
			this.name = name;
			this.points = points;
			this.date = date;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getPoints() {
			return points;
		}

		public void setPoints(int points) {
			this.points = points;
		}

		public LocalDate getDate() {
			return date;
		}

		public void setDate(LocalDate date) {
			this.date = date;
		}
	}
	
	public void add(HistoryEntry historyEntry) {
		history.addFirst(historyEntry);
        while(history.size()>10) {
        	history.removeLast();
        }
	}
	
	public void removeLast() {
		history.removeLast();
	}
	
	public void readFromFile() {
		try {
			File file = new File("history", "history.txt");
			if (file.exists()) {
				BufferedReader br = new BufferedReader(new FileReader(file));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				if (everything.length()<1) {
					return;
				}
				String[] lines = everything.split(";");
				for(String str : lines) {
					HistoryEntry current = new HistoryEntry(str.split(","));
					history.add(current);
				}
				br.close();
			}
		} catch (IOException e) { }
	}
	
	public void saveToFile() {
		try {
			File file = new File("history", "history.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			PrintWriter out = new PrintWriter(file);
			out.print(toString());
			out.close();
		} catch (IOException e) { }
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("");
		for (HistoryEntry historyEntry : history) {
			str.append(historyEntry.getName() + "," +
					historyEntry.getPoints()+ "," +
					historyEntry.getDate() + ";" );
		}

		if (str.length()>0) {
			str.deleteCharAt(str.length()-1);
		}
		return str.toString();
	}
}
