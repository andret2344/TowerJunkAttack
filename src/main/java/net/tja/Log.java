package net.tja;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
	public static void info(Object message) {
		System.out.println("[" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "] [INFO]: " + message);
	}

	public static void warn(Object message) {
		System.out.println("[" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "] [WARNING]: " + message);
	}

	public static void error(Object message) {
		System.out.println("[" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "] [ERROR]: " + message);
	}
}