package net.tja;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Location {
    private int x, y;

    public Location(Location l) {
        x = l.x;
        y = l.y;
    }
}
