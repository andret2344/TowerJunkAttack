package net.tja;

import net.tja.panel.PanelManager;

import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

/**
 * @author Panda
 */
public class TowerJunkAttack {

    private static final JPanel mainPanel = new JPanel();
    private static final JFrame frame = new JFrame();
    private static final boolean database = false; //XXX

    private static final AudioPlayer player = new AudioPlayer();
    private static Thread playerThread;
    private static boolean playerIsPlaying = false;
    private static boolean playerIsPause = false;

    private static final OperatingSystem system;

    private static int SIZE = 500;
    private static int volume = 80;
    private static Game game = new Game();
    private static History history = new History();

    static {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("linux")) {
            system = OperatingSystem.LINUX;
        } else if (os.contains("mac")) {
            system = OperatingSystem.MAC;
        } else if (os.contains("windows")) {
            system = OperatingSystem.WINDOWS;
        } else {
            system = null;
        }
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception Cos yes
     */
    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        history.readFromFile();
        if (!database) {
            String nickname;
            do {
                nickname = JOptionPane.showInputDialog("Podaj nick: ");
                if (nickname == null || nickname.equals("")) {
                    JOptionPane.showMessageDialog(null, "Nick cannot be empty!");
                }
            } while (nickname == null || nickname.equals(""));
            game.setPlayer(new Player(nickname));
            PanelManager.RIGHT_BAR.refreshPlayerInfo();
            PanelManager.RIGHT_BAR.refreshHealth();
            PanelManager.RIGHT_BAR.refreshCash();
        }
        // history.add(history.new HistoryEntry(game));
        // history.saveToFile();//should be on game ending
        frame.setBounds(4, 4, (int) (SIZE * 1.25) + system.getBorderSizeX(), SIZE + system.getBorderSizeY());
        frame.setTitle("Tower Junk Attack");
        frame.addComponentListener(new ComponentListener() {
            @Override
            public void componentHidden(ComponentEvent e) {
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentResized(ComponentEvent e) {
                SIZE = (int) frame.getBounds().getHeight() - system.getBorderSizeY();
                PanelManager.GAME.getGame().getMap().resizeBlock(SIZE);
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }
        });
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
            if (e.getID() == KeyEvent.KEY_RELEASED) {
                if (PanelManager.GAME.isVisible()) {
                    if (e.getKeyCode() == KeyEvent.VK_S) {
                        if (e.isControlDown()) {
                            if (game.getSaveName() == null) {
                                game.pause();
                                String saveName;
                                while (true) {
                                    saveName = JOptionPane.showInputDialog(null, "Save name: ");
                                    if (new File("saves", saveName + ".tja").exists()) {
                                        int dialogResult = JOptionPane.showConfirmDialog(null, "Save already exists. Overwrite?", "Warning", JOptionPane.YES_NO_OPTION);
                                        if (dialogResult == JOptionPane.NO_OPTION) {
                                            continue;
                                        }
                                    }
                                    break;
                                }
                                if (saveName != null && !saveName.equals("")) {
                                    game.setSaveName(saveName);
                                    JOptionPane.showMessageDialog(null, "Zapisano");
                                    Log.info("Saved game: \"" + saveName + "\"");
                                    game.unpause();
                                    game.save();
                                }
                            }
                        }
                    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        if (game.getSelectedTower() == null) {
                            if (game.isPaused()) {
                                game.unpause();
                            } else {
                                game.pause();
                            }
                        } else {
                            game.setSelectedTower(null);
                            PanelManager.RIGHT_BAR.removeTowerInfo();
                        }
                    }
                }
            }
            return false;
        });
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
        frame.setContentPane(mainPanel);
        mainPanel.add(database ? PanelManager.LOGIN : PanelManager.MAIN_MENU);
        frame.setVisible(true);
        playerThread = new Thread(() -> {
            try {
                player.loadFile("sounds" + File.separator + "music.wav");
                player.loop();
                player.play();
            } catch (UnsupportedAudioFileException | LineUnavailableException | IOException ex) {
                ex.printStackTrace();
            }

        });

        playerThread.start();

    }

    public static JPanel getPanel() {
        return mainPanel;
    }

    public static int getSize() {
        return SIZE;
    }

    public static Game getGame() {
        return game;
    }

    public static History getHistory() {
        return history;
    }

    public static OperatingSystem getSystem() {
        return system;
    }

    public static void pauseMusic() {
        if (playerIsPause) {
            playerIsPause = false;
            player.play();
        } else {
            playerIsPause = true;
            player.pause();
        }
        playerThread.interrupt();
    }

    public static boolean isMaximized() {
        return frame.getExtendedState() == JFrame.MAXIMIZED_BOTH;
    }

    public static int getSoundVolume() {
        return volume;
    }

    public static void setSoundVolume(int vol) {
        volume = vol;
        FloatControl gainControl = (FloatControl) player.getAudioClip().getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue((float) (20 * Math.log10(vol / 100.0)));
        playerThread.interrupt();
    }
}
