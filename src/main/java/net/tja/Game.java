package net.tja;

import net.tja.map.Map;
import net.tja.map.DataMapCell;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import net.tja.panel.PanelManager;
import net.tja.tower.Tower;
import net.tja.tower.TowerType;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andret
 */
public class Game {
	private Player player;
	private TowerType selectedTower;
	private Map map = loadMap("dupa");
	private boolean paused = true;
	private String saveName;
		
	Game() {
		map.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseReleased(MouseEvent e) {
				DataMapCell cell = map.getCellAt(e.getY(), e.getX());
				if (selectedTower!=null && cell!=null && cell.getData()==0) {
					int price = new Tower(selectedTower).getPrice();
					if (player.getCash()>=price) {
						cell.setData(selectedTower.getId());
						map.setDataMapTower(cell);
						player.setCash(player.getCash()-price);
						PanelManager.RIGHT_BAR.refreshCash();
						Log.info(selectedTower.name() + " tower placed at: " + cell.getLocation());
					} else {
						Log.error("Not enough money!");
					}
				}
			}
		});
	}

	public void startGame() {
		Log.info("Started");
		paused = false;
	}

	public String getSaveName() {
		return saveName;
	}

	public void pause() {
		Log.info("Paused");
		paused = true;
	}

	public void unpause() {
		Log.info("Unpaused");
		paused = false;
	}

	public void setSaveName(String saveName) {
		this.saveName = saveName;
	}

	public boolean isPaused() {
		return paused;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void setPlayer(Player player){
		this.player = player;
	}
	
	public Map getMap(){
		return map;
	}
	
	public void setMap(Map map){
		this.map = map;
	}

	public TowerType getSelectedTower(){
		return selectedTower;
	}
	
	public void setSelectedTower(TowerType selectedTower){
		this.selectedTower = selectedTower;
	}

	public void recreate() {
		map = loadMap("dupa");
	}

	public Map loadMap(String name) {
		try {
			File current = new File("maps", name + ".map");
			if (!current.exists()) {
				return null;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(current)));
			int sign;
			List<String> tmp = new ArrayList<String>();
			String curr = new String("");
			while ((sign = br.read())>0) {
				if ((char)sign!='\0' && (char)sign!='\n' && (char)sign!='\r') {
					curr += (char)sign;
				} else {
					if (!curr.equals("")) {
						tmp.add(curr);
					}
					curr = new String("");
				}
			}
			if (!curr.equals("")) {
				tmp.add(curr);
			}
			String[] lines = tmp.toArray(new String[]{});
			int[][] arr = new int[lines.length][];
			for (int i = 0; i<lines.length; i++) {
				char[] carr = lines[i].toCharArray();
				arr[i] = new int[carr.length];
				for (int j = 0; j<carr.length; j++) {
					arr[i][j] = Integer.parseInt(String.valueOf(carr[j]));
				}
			}
			br.close();
			return new Map(this, name, TowerJunkAttack.getSize(), arr);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public boolean save() {
		if (saveName==null) {
			return false;
		}
		try {
			JSONObject json = new JSONObject();
			JSONArray array = new JSONArray();
			File folder = new File("saves");
			if (!folder.exists()) {
				folder.mkdir();
			}
			Log.info(folder.exists());
			File file = new File("saves", saveName + ".tja");
			if (!file.exists()) {
				file.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			json.put("nick", player.getName());
			json.put("pts", player.getPoints());
			json.put("cash", player.getCash());
			json.put("hp", player.getHp());
			json.put("last_wave", 0);
			json.put("map", map.getName());
			for (Tower tower : map.getTowers()) {
				array.put(tower.toJSON());
			}
			json.put("towers", array);
			bw.write(json.toString());
			bw.close();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
}

